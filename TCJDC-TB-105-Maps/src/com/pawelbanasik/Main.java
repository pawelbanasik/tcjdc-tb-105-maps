package com.pawelbanasik;

import java.util.HashMap;
import java.util.Map;

public class Main {

	public static void main(String[] args) {

		Map<String, String> languages = new HashMap<>();
		if (languages.containsKey("Java")) {
			System.out.println("Java already exists.");
		} else {
			languages.put("Java", "a compiled high-level, object-oriented language");
			System.out.println("Java added succesfully");
		}

		// dodawanie do mapy
		languages.put("Python", "an interpreted, object-oriented language");
		languages.put("Algol", "an algorythmic language");

		// sprawdzenie czy jest w mapie
		System.out.println(languages.put("Basic", "Begginer all purpose"));

		// sprawdzenie czy jest taki klucz
		if (languages.containsKey("Java")) {
			System.out.println("Java is already in the map");
		} else {
			languages.put("Java", "this course is about Java");

		}

		// usuwanie
		languages.remove("Algol");
		
//      languages.remove("Lisp");
      if(languages.remove("Algol", "an algorithmic language")) {
          System.out.println("Algol removed");
      } else {
          System.out.println("Algol not removed, key/value pair not found");
      }

      if(languages.replace("Lisp", "Therein lies madness", "a functional programming language with imperative features")) {
          System.out.println("Lisp replaced");
      } else {
          System.out.println("Lisp was not replaced");
      }
		
		
		// iteracja po kluczach
		for (String key : languages.keySet()) {
			System.out.println(key + " : " + languages.get(key));
		}

	}

}
